<?php

class Post {
    public static int $allPosts = 0;
    public int $id;
    public string $title;
    public string $description;
    public int $views = 0;
    public string $content;
    public ?int $author_id;

    public function  __construct(){
    }

    public function incrementViews(){
        $this->views++;
    }

    public function author(){
        if($this->author_id){
            $author_id = $this->author_id;
            $user = db()->getOne("SELECT * FROM users WHERE id = $author_id", "User");
            return $user;
        }
        $user =  new User();
        $user->name = "Автору жок";
        return $user;
    }
}