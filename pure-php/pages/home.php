<?php
$posts = db()->getPosts();
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.1/css/bootstrap.min.css"
        integrity="sha384-VCmXjywReHh4PwowAiWNagnWcLhlEJLA5buUprzK8rxFgeH0kww/aWY76TfkUoSX" crossorigin="anonymous" />
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" />
    <link rel="stylesheet" href="css/styles.css" />
    <title>Document</title>
</head>

<body>
    <?php include "./pages/partials/header.php"; ?>

    <div class="banner text-center">
        <h1 class="display-4 mb-4 banner-title">
            Максаттарга кандай жетүү керек?
        </h1>

        <p class="mb-5 banner-content">
            The 5 regrets paint a portrait of post-industrial man, who shrinks
            himself into a shape that fits his circumstances, then turns dutifully
            till he stops.
        </p>

        <div>
            <a href="post-details.html" class="btn btn-success mr-2">Толук окуп чыгуу</a>
            <a class="btn btn-primary" href="/new-post">Кошуу</a>
        </div>
        <div class="blur" id="particle-js"></div>
    </div>

    <div class="container">
        <div class="posts py-5">
            <div class="row">

                <?php foreach($posts as $post): ?>
                <div class="col-lg-4 col-md-6 col-12 mb-3">
                    <div class="card">
                        <a href="/post/<?= $post->id ?>">
                            <img src="https://via.placeholder.com/720x480" class="card-img-top" alt="" />
                        </a>
                        <div class="card-body d-flex flex-row p-2">
                            <div>
                                <img src="https://via.placeholder.com/70" alt="" />
                            </div>
                            <div class="ml-2">
                                <h5><a href="/post/<?= $post->id ?>"><?= $post->title ?></a></h5>
                                <p class="text-muted"><?= $post->description ?></p>
                            </div>
                        </div>
                        <div class="card-decoration"></div>
                        <div class="card-decoration"></div>
                    </div>
                </div>
                <?php endforeach; ?>
            </div>
        </div>

        <div class="qutoes">
            <hr />
            <div class="row">
                <div class="col-lg-4 col-md-6 col-12">
                    <div class="quote d-flex flex-row">
                        <div>
                            <img class="rounded-circle" src="https://via.placeholder.com/100" alt="" />
                        </div>
                        <div class="p-2">
                            <p class="m-0 text-muted">Oct 15</p>
                            <p class="m-0">
                                Lorem ipsum dolor sit amet consectetur adipisicing elit.
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-12">
                    <div class="quote d-flex flex-row">
                        <div>
                            <img class="rounded-circle" src="https://via.placeholder.com/100" alt="" />
                        </div>
                        <div class="p-2">
                            <p class="m-0 text-muted">Oct 15</p>
                            <p class="m-0">
                                Lorem ipsum dolor sit amet consectetur adipisicing elit.
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-12">
                    <div class="quote d-flex flex-row">
                        <div>
                            <img class="rounded-circle" src="https://via.placeholder.com/100" alt="" />
                        </div>
                        <div class="p-2">
                            <p class="m-0 text-muted">Oct 15</p>
                            <p class="m-0">
                                Lorem ipsum dolor sit amet consectetur adipisicing elit.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <hr />
        </div>

        <div class="featured mb-5">
            <div class="row">
                <div class="col-lg-2 col-12">
                    <span>Featured on:</span>
                </div>
                <div class="col-lg-10 col-12">
                    <a class="d-inline-block m-2" href="">
                        <img src="https://via.placeholder.com/150x70" alt="" />
                    </a>
                    <a class="d-inline-block m-2" href="">
                        <img src="https://via.placeholder.com/150x70" alt="" />
                    </a>
                    <a class="d-inline-block m-2" href="">
                        <img src="https://via.placeholder.com/150x70" alt="" />
                    </a>
                    <a class="d-inline-block m-2" href="">
                        <img src="https://via.placeholder.com/150x70" alt="" />
                    </a>
                    <a class="d-inline-block m-2" href="">
                        <img src="https://via.placeholder.com/150x70" alt="" />
                    </a>
                </div>
            </div>
        </div>
    </div>

    <footer class="px-5 py-4 bg-dark">
        <div class="row">
            <div class="col-lg-6 col-12">
                <div class="row">
                    <div class="col-lg-3 col-md-6 col-12">
                        <h4 class="">Main</h4>
                        <ul>
                            <li>
                                <a href="#">Start here</a>
                            </li>
                            <li>
                                <a href="#">Start here</a>
                            </li>
                            <li>
                                <a href="#">Start here</a>
                            </li>
                            <li>
                                <a href="#">Start here</a>
                            </li>
                            <li>
                                <a href="#">Start here</a>
                            </li>
                        </ul>
                    </div>
                    <div class="col-lg-3 col-md-6 col-12">
                        <h4 class="">Main</h4>
                        <ul>
                            <li>
                                <a href="#">Start here</a>
                            </li>
                            <li>
                                <a href="#">Start here</a>
                            </li>
                            <li>
                                <a href="#">Start here</a>
                            </li>
                            <li>
                                <a href="#">Start here</a>
                            </li>
                            <li>
                                <a href="#">Start here</a>
                            </li>
                        </ul>
                    </div>
                    <div class="col-lg-3 col-md-6 col-12">
                        <h4 class="">Main</h4>
                        <ul>
                            <li>
                                <a href="#">Start here</a>
                            </li>
                            <li>
                                <a href="#">Start here</a>
                            </li>
                            <li>
                                <a href="#">Start here</a>
                            </li>
                            <li>
                                <a href="#">Start here</a>
                            </li>
                            <li>
                                <a href="#">Start here</a>
                            </li>
                        </ul>
                    </div>
                    <div class="col-lg-3 col-md-6 col-12">
                        <h4 class="">Main</h4>
                        <ul>
                            <li>
                                <a href="#">Start here</a>
                            </li>
                            <li>
                                <a href="#">Start here</a>
                            </li>
                            <li>
                                <a href="#">Start here</a>
                            </li>
                            <li>
                                <a href="#">Start here</a>
                            </li>
                            <li>
                                <a href="#">Start here</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-12 text-lg-right">
                <div class="float-lg-right">
                    <div class="social-links">
                        <ul>
                            <li>
                                <a href="#">
                                    <i class="fa fa-facebook"></i>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <i class="fa fa-twitter"></i>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <i class="fa fa-instagram"></i>
                                </a>
                            </li>
                        </ul>
                    </div>
                    <div class="copyright">
                        &copy; 2020 Be Happy
                        <p>
                            <a href="">Privacy Policy</a>
                            <span>&#9679;</span>
                            <a href="">Terms of Service</a>

                        </p>
                    </div>
                </div>
            </div>
        </div>
    </footer>


    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"
        integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous">
    </script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"
        integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous">
    </script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.1/js/bootstrap.min.js"
        integrity="sha384-XEerZL0cuoUbHE4nZReLT7nx9gQrQreJekYhJD9WNWhH8nEW+0c5qq7aIo2Wl30J" crossorigin="anonymous">
    </script>

    <!-- <script src="https://cdn.jsdelivr.net/npm/particles.js@2.0.0/particles.min.js"></script> -->
    <!-- <script src="js/partice-config.js"></script> -->
</body>

</html>