<?php
$message = null;
if($_GET['message']){
    $message = $_GET['message'];
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.1/css/bootstrap.min.css"
        integrity="sha384-VCmXjywReHh4PwowAiWNagnWcLhlEJLA5buUprzK8rxFgeH0kww/aWY76TfkUoSX" crossorigin="anonymous" />
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" />
    <link rel="stylesheet" href="css/styles.css" />
    <title>Document</title>
</head>

<body>
    <?php include "./pages/partials/header.php"; ?>

    <div class="container">
        <div class="mt-5">
            <h3>Жаңы пост</h3>
        </div>
        <form class="needs-validation my-5" novalidate action="/new-post" method="POST">
            <?php if($message != null): ?>
            <div class="alert alert-danger">
                <p><?= $message ?></p>
            </div>
            <?php endif; ?>
            <div class="mb-3">
                <label for="email">Посттун аталышы</label>
                <input type="text" class="form-control" name="title" required="" placeholder="">
                <div class="invalid-feedback">
                    Атыңызды жазыңыз.
                </div>
            </div>
            <div class="mb-3">
                <label for="email">Поттун сүрөттөлүшү</label>
                <input type="text" class="form-control" required="" name="description" placeholder="">
                <div class="invalid-feedback">
                    Электрондук дарегиңизди жазыңыз.
                </div>
            </div>
            <div class="mb-3">
                <label for="address">Посттун мазмуну</label>
            </div>
            <div class="mb-3">
                <textarea class="form-control" name="content" id="" rows="20"></textarea>
                <div class="invalid-feedback">
                    Сырсөзүңүздү киргизиңиз.
                </div>
            </div>
            <button class="btn btn-primary" type="submit">Сактоо</button>
        </form>

    </div>

    <footer class="px-5 py-4 bg-dark">
        <div class="row">
            <div class="col-lg-6 col-12">
                <div class="row">
                    <div class="col-lg-3 col-md-6 col-12">
                        <h4 class="">Main</h4>
                        <ul>
                            <li>
                                <a href="#">Start here</a>
                            </li>
                            <li>
                                <a href="#">Start here</a>
                            </li>
                            <li>
                                <a href="#">Start here</a>
                            </li>
                            <li>
                                <a href="#">Start here</a>
                            </li>
                            <li>
                                <a href="#">Start here</a>
                            </li>
                        </ul>
                    </div>
                    <div class="col-lg-3 col-md-6 col-12">
                        <h4 class="">Main</h4>
                        <ul>
                            <li>
                                <a href="#">Start here</a>
                            </li>
                            <li>
                                <a href="#">Start here</a>
                            </li>
                            <li>
                                <a href="#">Start here</a>
                            </li>
                            <li>
                                <a href="#">Start here</a>
                            </li>
                            <li>
                                <a href="#">Start here</a>
                            </li>
                        </ul>
                    </div>
                    <div class="col-lg-3 col-md-6 col-12">
                        <h4 class="">Main</h4>
                        <ul>
                            <li>
                                <a href="#">Start here</a>
                            </li>
                            <li>
                                <a href="#">Start here</a>
                            </li>
                            <li>
                                <a href="#">Start here</a>
                            </li>
                            <li>
                                <a href="#">Start here</a>
                            </li>
                            <li>
                                <a href="#">Start here</a>
                            </li>
                        </ul>
                    </div>
                    <div class="col-lg-3 col-md-6 col-12">
                        <h4 class="">Main</h4>
                        <ul>
                            <li>
                                <a href="#">Start here</a>
                            </li>
                            <li>
                                <a href="#">Start here</a>
                            </li>
                            <li>
                                <a href="#">Start here</a>
                            </li>
                            <li>
                                <a href="#">Start here</a>
                            </li>
                            <li>
                                <a href="#">Start here</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-12 text-lg-right">
                <div class="float-lg-right">
                    <div class="social-links">
                        <ul>
                            <li>
                                <a href="#">
                                    <i class="fa fa-facebook"></i>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <i class="fa fa-twitter"></i>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <i class="fa fa-instagram"></i>
                                </a>
                            </li>
                        </ul>
                    </div>
                    <div class="copyright">
                        &copy; 2020 Be Happy
                        <p>
                            <a href="">Privacy Policy</a>
                            <span>&#9679;</span>
                            <a href="">Terms of Service</a>

                        </p>
                    </div>
                </div>
            </div>
        </div>
    </footer>


    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"
        integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous">
    </script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"
        integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous">
    </script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.1/js/bootstrap.min.js"
        integrity="sha384-XEerZL0cuoUbHE4nZReLT7nx9gQrQreJekYhJD9WNWhH8nEW+0c5qq7aIo2Wl30J" crossorigin="anonymous">
    </script>

    <script src="js/main.js"></script>
</body>

</html>