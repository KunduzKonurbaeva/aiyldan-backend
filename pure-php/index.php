<?php
include_once './Database.php';
session_start();
$db = null;

function db(){
    if($db == null){
        $db = Database::getInstance();
    }
    return $db;
}

function redirect($uri){
    header("Location: " . $uri);
}

$uri = $_SERVER['REQUEST_URI'];
$method = $_SERVER['REQUEST_METHOD'];

$authorizedUser = null;
if($_SESSION['user']){
    $authorizedUser = $_SESSION['user'];
}

if($uri == "/" && $method == "GET"){
    include "./pages/home.php";
}else if($uri == "/posts" && $method == "GET"){
    include "./pages/posts.php";
}else if(preg_match("/\/post\/[0-9]+/", $uri) && $method == "GET"){
    include "./pages/read_post.php";
}else if($uri == "/logout" && $method == "GET"){
    include "./pages/logout.php";
}else if(preg_match("/\/login.*/", $uri) && $method == "GET"){
    include "./pages/login.php";
}else if(preg_match("/\/login.*/", $uri) && $method == "POST"){
    include "./pages/login_handler.php";
}else if(preg_match("/\/new-post.*/", $uri) && $method == "GET"){
    include "./pages/new_post.php";
}else if(preg_match("/\/delete-post.*/", $uri) && $method == "GET"){
    include "./pages/delete_post.php";
}else if(preg_match("/\/new-post.*/", $uri) && $method == "POST"){
    include "./pages/new_post_handler.php";
}else if(preg_match("/\/register.*/", $uri) && $method == "GET"){
    include "./pages/register.php";
}else if(preg_match("/\/register.*/", $uri) && $method == "POST"){
    include "./pages/register_handler.php";
}else {
    include "./pages/error_404.php";
}
