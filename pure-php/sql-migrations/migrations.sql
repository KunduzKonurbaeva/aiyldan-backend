
-- таблицаны өчүрүү
DROP TABLE posts;

-- Маалымат базасына таблица кошуу
CREATE TABLE users (
	id INT PRIMARY KEY AUTO_INCREMENT,
	name varchar(255) NOT NULL,
	login varchar(255) NOT NULL UNIQUE,
	password varchar(255) NOT NULL
);

CREATE TABLE posts (
	id INT PRIMARY KEY AUTO_INCREMENT,
	title varchar(255) NOT NULL,
	description text,
	content text NOT NULL,
	author_id INT,
-- 	эгер аталык объект өчүп кетсе NULL кылып салуу
-- 	FOREIGN KEY(author_id) REFERENCES users(id) ON DELETE SET NULL
	FOREIGN KEY(author_id) REFERENCES users(id) ON DELETE CASCADE
);


-- Маалымат базасын кошуу
CREATE DATABASE alatoo2;


-- Маалмат базасын тандоо
USE alatoo;


SELECT * FROM users;

-- маалымат алуу үчүн
SELECT * FROM posts ORDER BY id DESC;

-- Маалыат кошуу
INSERT INTO users (name, login, password) VALUES ('Kairat', 'kairat', 'turat123');

INSERT INTO posts (title, description, content, author_id) VALUES ('DBeaver аркылуу кошулган пост', 'түшүндүрмө текст', 'text lorem', 5);

-- Белгилүү бир объектии көрсөтүү
SELECT * FROM posts WHERE id = 3;

-- Белгилүү бир объектти өчүрүү
DELETE FROM posts WHERE id = 2;

DELETE FROM users WHERE id = 4;

-- Белгилүү id 4 болгон объектти өзгөртүү
UPDATE posts SET title = 'Kalp ele', description = 'desc' WHERE id = 4;

SELECT * FROM posts;

SELECT * FROM posts WHERE title LIKE "%блогум";

SELECT * FROM posts WHERE title LIKE "мен%";

SELECT * FROM posts WHERE title NOT LIKE "мен%";

SELECT * FROM posts WHERE id != 8;




INSERT INTO posts (title, description, content) VALUES ('DBeaver аркылуу кошулган пост', 'түшүндүрмө текст', 'text lorem');



