<?php
include_once './models/User.php';
include_once './models/Post.php';

class Database {

    private static ?Database $db = null;
    private PDO $pdo;
    

    public static function getInstance(){
        if(self::$db == null){
            self::$db = new Database();
            try{
                $host = "localhost:3306";
                $dbname = "alatoo";
                $username = "root";
                $password = "erke123";
            
                self::$db->pdo = new PDO("mysql:host=$host;dbname=$dbname", $username, $password);
                self::$db->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            
            }catch(PDOException $error){
                echo $error;
            }
        }
        return self::$db;
    }

    public function getAll(string $query, string $className){
        $stmt = $this->pdo->prepare($query);
        $stmt->execute();
        return $stmt->fetchAll(PDO::FETCH_CLASS, $className);
    }

    public function getOne(string $query, string $className){
        $stmt = $this->pdo->prepare($query);
        $stmt->execute();
        return $stmt->fetchObject($className);
    }

    public function query(string $query){
        $stmt = $this->pdo->prepare($query);
        return $stmt->execute();
    }

    public function queryWithData(string $query, $data){
        $stmt = $this->pdo->prepare($query);
        return $stmt->execute($data);
    }

    public function getPosts(){
        return $this->getAll("SELECT * FROM posts ORDER BY id DESC", "Post");
    }

    public function getUsers(){
        return $this->getAll("SELECT * FROM users", "User");
    }
}