<?php

class Calendar{
    private Array $monthes;

    public function __construct(){
        $this->monthes = [
            "Jan",
            "Feb",
            "Mart",
            "Apr",
            "May",
            "June",
            "Jule",
            "Aug",
            "Sep",
            "Oct",
            "Nov",
            "Dec",
        ];

    }


    /**
     * number between 1 and 12
     */
    public function getMonth(int $number){
        if($number >= 1 && $number <= 12){
            return $this->monthes[$number - 1];
        }else{
            throw new OutOfBoundsException("Сан туура эмес берилди");
        }
    }
}



$calendar = new Calendar();


try{
    echo $calendar->getMonth(122);
}catch(Exception $er){
    echo "Ката чыкты";
}